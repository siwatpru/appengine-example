import os
import webapp2
import jinja2
from models.greeting import Greeting
from google.appengine.ext import ndb

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'templates'))),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class MainPage(webapp2.RequestHandler):
    def get(self):
        # query entity
        greetings = Greeting.query(ancestor=ndb.Key("greetings",1)).order(Greeting.time)
        
        # create dictionary for template rendering
        template_values = {}
        template_values['greetings'] = greetings
        template_values['aaa'] = True
        template_values['bbb'] = False
        template = JINJA_ENVIRONMENT.get_template('template.html')
        self.response.write(template.render(template_values))


application = webapp2.WSGIApplication([
    ('/*', MainPage)
])