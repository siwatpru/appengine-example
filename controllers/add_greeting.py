import webapp2
from models.greeting import Greeting
from google.appengine.ext import ndb

class AddGreeting(webapp2.RequestHandler):
    def post(self):
        greeting = self.request.POST.get('greeting')
        Greeting(
            parent=ndb.Key("greetings",1),
            message = greeting
            ).put()
        self.redirect('/')

application = webapp2.WSGIApplication([
    ('/add/?', AddGreeting)
])