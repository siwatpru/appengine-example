from google.appengine.ext import ndb

class Greeting(ndb.Model):
    message = ndb.StringProperty(required=True)
    time = ndb.DateTimeProperty(auto_now_add=True)